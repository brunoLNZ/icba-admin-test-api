var express = require('express'),
    app = express(),
    bodyParser = require('body-parser'),
    methodOverride = require('method-override'),
    morgan = require('morgan');

app.use(bodyParser.urlencoded({ extended: false, limit: '5mb' }));  
app.use(bodyParser.json({ limit: '5mb' }));
app.use(methodOverride());
app.use(morgan('combined'));

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Token');
  next();
});

var router = express.Router();

var delay = 2000;

router.get('/', function(req, res) {  
  res.send('Hello World!!!');
});

router.post('/InterfacesNetWeb/rest/ping', function(req, res) {
  var impl = function () {
    res.json({ RespuestaCodigo: 0, respuestaMensaje: 'OK' });
  };
  setTimeout(impl, delay);
});

router.post('/InterfacesNetWeb/rest/logInMedicoREST', function(req, res) {
  var impl = function () {
    if(req.body.Usuario != 'SDMIGUEL') {
      res.json({Token: '', Nombre: '', RespuestaCodigo: 2, respuestaMensaje: 'Usuario inexistente'});
    } 
    else if(req.body.Clave != '1234') {
      res.json({Token: '', Nombre: '', RespuestaCodigo: 1, respuestaMensaje: 'Contraseña incorrecta'});
    }
    else {
      res.json({Token: 'OBPXo3IjSwgVB4ZkIMoyKlia948=', Nombre: 'FERRADAS, Silvina', RespuestaCodigo: 0, respuestaMensaje: 'OK'});
    }
  };
  setTimeout(impl, delay);
});

router.post('/InterfacesNetWeb/rest/getMedicoPacientesREST', function(req, res) {
  var impl = function () {
    var token = req.header('Token');
    if(token != 'OBPXo3IjSwgVB4ZkIMoyKlia948=') {
      res.status(500);
      res.send('ERROR');
      return;
    }

    var pacientes = [
      {
        NombreCompleto: 'GOMES, FLORENCIA',
        HistoriaClinica: '200617',
        HistoriaClinicaInterna: '213863',
        FechaNacimiento: '1953-05-29',
        DNI: 'DNI 10962286',
        Cobertura: '461 OSDE (PREST. 250)',
        Afilliado: '61292597701',
        Turno: '15:00'
      },
      {
        NombreCompleto: 'GOMEZ, FLORENCIO',
        HistoriaClinica: '200615',
        HistoriaClinicaInterna: '213861',
        FechaNacimiento: '1983-05-29',
        DNI: 'DNI 32962286',
        Cobertura: '410 OSDE (PREST. 210)',
        Afilliado: '61292597751',
        Turno: '17:00'
      }
    ];
    res.json({
      DatosPaciente: pacientes,
      RespuestaCodigo: 0,
      respuestaMensaje: 'OK'
    });
  };
  setTimeout(impl, delay);
});

router.post('/InterfacesNetWeb/rest/validaTokenREST', function(req, res) {
  var impl = function () {
    var token = req.header('Token');
    if(token == 'OBPXo3IjSwgVB4ZkIMoyKlia948=') {
      res.json({ RespuestaCodigo: 0, respuestaMensaje: 'OK' });
    }
    else {
      res.json({ RespuestaCodigo: 4, respuestaMensaje: 'Sessión caducada' });
    }
  };
  setTimeout(impl, delay);
});

router.post('/InterfacesNetWeb/rest/getPacientesREST', function(req, res) {
  var impl = function () {
    var token = req.header('Token');
    if(token != 'OBPXo3IjSwgVB4ZkIMoyKlia948=') {
      res.status(500);
      res.send('ERROR');
      return;
    }

    var pacientes = []
    if(req.body.PacienteBuscado != 99) {
      if(req.body.PacienteBuscado == 200617) {
        pacientes = [
          {
            NombreCompleto: 'GOMES, FLORENCIA',
            HistoriaClinica: '200617',
            HistoriaClinicaInterna: '213863',
            FechaNacimiento: '1953-05-29',
            DNI: 'DNI 10962286',
            Cobertura: '461 OSDE (PREST. 250)',
            Afilliado: '61292597701'
          }
        ]
      }
      else if(req.body.PacienteBuscado == 200615) {
        pacientes = [
          {
            NombreCompleto: 'GOMEZ, FLORENCIO',
            HistoriaClinica: '200615',
            HistoriaClinicaInterna: '213861',
            FechaNacimiento: '1983-05-29',
            DNI: 'DNI 32962286',
            Cobertura: '410 OSDE (PREST. 210)',
            Afilliado: '61292597751'
          }
        ]
      }
      else {
        pacientes = [
          {
            NombreCompleto: 'GOMEZ, JORGE',
            HistoriaClinica: '120937',
            HistoriaClinicaInterna: '2969',
            FechaNacimiento: '1957-03-12',
            DNI: 'DNI 12798464',
            Cobertura: '461 OSDE (PREST. 250)',
            Afilliado: '60 843513 8 01',
          },
          {
            NombreCompleto: 'GOMEZ, JORGE ALBERTO',
            HistoriaClinica: '125937',
            HistoriaClinicaInterna: '122815',
            FechaNacimiento: '1957-03-12',
            DNI: 'DNI 12798464',
            Cobertura: '461 OSDE (PREST. 250)',
            Afilliado: '60843513801',
          }
        ]
      }
    }

    var data = {
      DatosPaciente: pacientes,
      RespuestaCodigo: 0,
      respuestaMensaje: 'OK'
    };

    res.json(data);
  };

  setTimeout(impl, delay);
});

router.post('/InterfacesNetWeb/rest/getEstudiosREST', function(req, res) {
  var impl = function () {
    var token = req.header('Token');
    if(token == 'OBPXo3IjSwgVB4ZkIMoyKlia948=') {
      res.json({ 
        EstudiosExternos: [
          {
            Id: 1,
            Descripcion: 'Alta',
            sdtEstudiosRestItem: [
              {
                Id: 1,
                Descripcion: 'DNI'
              },
              {
                Id: 2,
                Descripcion: 'Credencial'
              },
              {
                Id: 3,
                Descripcion: 'Poder'
              },
              {
                Id: 4,
                Descripcion: 'Comprobante de pago'
              },
            ]
          },
          {
            Id: 2,
            Descripcion: 'Documentación estudios',
            sdtEstudiosRestItem: [
              {
                Id: 1,
                Descripcion: 'Orden'
              },
              {
                Id: 2,
                Descripcion: 'Autorización Obra Social'
              },
              
            ]
          },
          {
            Id: 3,
            Descripcion: 'Registro de pago',
            sdtEstudiosRestItem: [
              {
                Id: 1,
                Descripcion: 'Comprobante'
              },
              {
                Id: 2,
                Descripcion: 'Factura'
              },
            ]
          }
        ],
        RespuestaCodigo: 0,
        respuestaMensaje: 'OK'
      });
    }
    else {
      res.json({ RespuestaCodigo: 4, respuestaMensaje: 'Sessión caducada' });
    }
  };
  setTimeout(impl, delay);
});


router.post('/InterfacesNetWeb/rest/setEstudioImagenREST', function(req, res) {
  var impl = function () {
    var token = req.header('Token');
    if(token != 'OBPXo3IjSwgVB4ZkIMoyKlia948=') {
      res.status(500);
      res.send('ERROR');
      return;
    }

    var fail = Math.floor(Math.random() * 99) % 2 == 0;
    if(fail) {
      res.status(500);
      res.send('ERROR');
      return;
    }

    var data = {
      EstudioNumero: '19',
      RespuestaCodigo: 0,
      respuestaMensaje: 'OK'
    };

    res.json(data);
  };

  setTimeout(impl, 3000);
});

router.post('/InterfacesNetWeb/rest/getEstudiosPacienteREST', function(req, res) {
  var impl = function () {
    var token = req.header('Token');
    if(token != 'OBPXo3IjSwgVB4ZkIMoyKlia948=') {
      res.status(500);
      res.send('ERROR');
      return;
    }

    var data = {
      EstudiosPaciente: [
        {
          estudioNumero: '1317693',
          estudioGrupo: 1,
          estudioGrupoDsc: 'Estudios Diagnósticos',
          estudioId: 3,
          estudioDsc: 'RAYOS',
          estudioFecha: '2017-05-12',
          estudioObservacion: '',
          imagenes: 3
        },
        {
          estudioNumero: '1317694',
          estudioGrupo: 1,
          estudioGrupoDsc: 'Estudios Diagnósticos',
          estudioId: 2,
          estudioDsc: 'ECG',
          estudioFecha: '2017-05-13',
          estudioObservacion: 'comentarios',
          imagenes: 1
        }
      ],
      RespuestaCodigo: 0,
      respuestaMensaje: 'OK'
    };

    res.json(data);
  };

  setTimeout(impl, delay);
});

router.post('/InterfacesNetWeb/rest/getImagenEstudioPacienteREST', function(req, res) {
  var impl = function () {
    var token = req.header('Token');
    if(token != 'OBPXo3IjSwgVB4ZkIMoyKlia948=') {
      res.status(500);
      res.send('ERROR');
      return;
    }

    var data = {
      EstudioPaciente: {
        estudioNumero: 1317693,
        estudioGrupo: 1,
        estudioGrupoDsc: 'Estudios Diagnósticos',
        estudioId: 3,
        estudioDsc: 'RAYOS',
        estudioFecha: '2017-05-12',
        estudioObservacion: 'estudio del paciente JORGE',
        imagenes: 4
      },
      Imagenes: [
        {
          ImagenNumero: 1,
          ImagenBlob: loadImageAsBase64String('bugger.jpg')
        },
        {
          ImagenNumero: 2,
          ImagenBlob: loadImageAsBase64String('drink.jpg')
        },
        {
          ImagenNumero: 3,
          ImagenBlob: loadImageAsBase64String('entree.jpg')
        },
        {
          ImagenNumero: 4,
          ImagenBlob: loadImageAsBase64String('burn.jpg')
        }
      ],
      RespuestaCodigo: 0,
      respuestaMensaje: 'OK'
    };

    res.json(data);
  };

  setTimeout(impl, delay);
});

router.post('/InterfacesNetWeb/rest/delEstudioCompletoREST', function(req, res) {
  var impl = function () {
    var token = req.header('Token');
    if(token != 'OBPXo3IjSwgVB4ZkIMoyKlia948=') {
      res.status(500);
      res.send('ERROR');
      return;
    }

    var data = {
      RespuestaCodigo: 0,
      respuestaMensaje: 'OK'
    };

    res.json(data);
  };

  setTimeout(impl, delay);
});

router.post('/InterfacesNetWeb/rest/delImagenEstudioREST', function(req, res) {
  var impl = function () {
    var token = req.header('Token');
    if(token != 'OBPXo3IjSwgVB4ZkIMoyKlia948=') {
      res.status(500);
      res.send('ERROR');
      return;
    }

    var data = {
      RespuestaCodigo: 0,
      respuestaMensaje: 'OK'
    };

    res.json(data);
  };

  setTimeout(impl, delay);
});

app.use(router);

var port = 8080;
app.listen(port, function() {  
  console.log('Node server running on http://localhost:' + port);
});

var fs = require('fs');

function loadImageAsBase64String(fileName, callback) {
  var bitmap = fs.readFileSync('imgs/'+fileName);
  return new Buffer(bitmap).toString('base64');
}
